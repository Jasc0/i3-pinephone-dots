# i3-pinephone-dots

## included scripts (add to your path in order to function)
### dimmer (screen control):

    requires:
    dmenu, light
### screen-toggle (screen lock)
    requires:
    dmenu, light, cpupower

    extra:
    ADD "user ALL=(root) NOPASSWD: /usr/bin/cpupower" TO END OF SUDOERS FILE
### flashlight (flashlight)
    requires:
    None
    
### statusbar (status bar command)
    requires:
    None

